package com.bodus.oauth.client;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
public class OAuth2Config {
    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Bean
    @ConfigurationProperties("security.oauth2.client")
    protected AuthorizationCodeResourceDetails oAuthDetails() {
        return new AuthorizationCodeResourceDetails();
    }
}
