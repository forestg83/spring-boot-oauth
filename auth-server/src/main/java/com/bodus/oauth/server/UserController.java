package com.bodus.oauth.server;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {

    @GetMapping("/me")
    public Principal getUser(Principal principal) {
        return principal;
    }
}